import React, { useContext } from 'react';
import styles from './styles.module.css'
import ComponentF from '../ComponentF';
import CounterContext from '../../contexts/counter';

const ComponentE = () => {
  const count = useContext(CounterContext);
  return (
    <CounterContext.Consumer>
      {(counter) => (
    <div className={styles.container}>
      ComponentE<br />
      Count: {counter.count}<br />
      <button onClick={counter.increment}>+</button>
      <button onClick={counter.decrement}>-</button>
      <br></br>
      <ComponentF
        count={count}
        increment={counter.increment}
        decrement={counter.decrement}
      />
    </div>
    )}
    </CounterContext.Consumer>
  );
}

export default ComponentE;
