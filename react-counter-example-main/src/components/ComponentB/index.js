import React, { useContext } from 'react';
import styles from './styles.module.css';
import ComponentD from '../ComponentD';
import CounterContext from '../../contexts/counter';

const ComponentB = () => {
  const value = useContext(CounterContext);
  return (
    <CounterContext.Consumer>
      {(counter) => (
    <div className={styles.container}>
      ComponentB<br />
      Count: {value.count}
      <ComponentD />
    </div>
      )}
      </CounterContext.Consumer>
  )
};

export default ComponentB;

