import React, { useState } from 'react';

import styles from './styles.module.css';

import ComponentB from '../ComponentB';
import ComponentC from '../ComponentC';
import CounterContext from '../../contexts/counter';

const ComponentA = () => {
  const [count, setCount] = useState(0);

  const increment = () => {
    return setCount((prevCount) => (prevCount + 1));
  };

  const decrement = () => {
    if (count > 0) {
      return setCount((prevCount) => (prevCount - 1));
    } 
  };

  return (
    <CounterContext.Provider value ={{ count, increment, decrement }}>
    <div className={styles.container}>
      ComponentA<br />
      Count: {count}
      <ComponentB />
      <ComponentC />
    </div>
    </CounterContext.Provider>
  );
};

export default ComponentA;