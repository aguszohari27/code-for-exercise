import React, { useContext } from 'react';
import styles from './styles.module.css';
import CounterContext from '../../contexts/counter';

const ComponentG = () => {
  const value = useContext(CounterContext);
  return (
    <CounterContext.Consumer>
      {(counter) => (
    <div className={styles.container}>
      ComponentG<br />
      Count: {value.count}<br />
      <button onClick={counter.increment}>+</button>
      <button onClick={counter.decrement}>-</button>
    </div>
    )}
    </CounterContext.Consumer>
  );
}

export default ComponentG;
