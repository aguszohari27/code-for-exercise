import React, { useContext } from 'react';
import CounterContext from '../../contexts/counter';
import styles from './styles.module.css';

const ComponentC = () => {
  const counter = useContext(CounterContext);
  return (
      <div className={styles.container}>
      ComponentC<br />
      Count: {counter.count}<br />
      <button onClick={counter.increment}>+</button>
      <button onClick={counter.decrement}>-</button>
    </div>
  );
}

export default ComponentC;
