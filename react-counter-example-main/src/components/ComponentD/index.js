import React, { useContext } from 'react';
import styles from './styles.module.css'
import CounterContext from '../../contexts/counter';
import ComponentE from '../ComponentE';

const ComponentD = () => {
  const count = useContext(CounterContext);

  return (
    <CounterContext.Consumer>
      {(counter) => (
        <div className={styles.container}>
        ComponentD<br />
        Count: {counter.count}<br />
        <button onClick={counter.increment}>+</button>
        <button onClick={counter.decrement}>-</button>
        <br></br>
        <ComponentE
        count={count}
        increment={counter.increment}
        decrement={counter.decrement}
        />
      </div>
      )}
      </CounterContext.Consumer>
  );
}

export default ComponentD;