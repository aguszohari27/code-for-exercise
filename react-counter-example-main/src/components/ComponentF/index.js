import React, { useContext } from 'react';
import styles from './styles.module.css';
import ComponentG from '../ComponentG';
import CounterContext from '../../contexts/counter';

const ComponentF = () => {
  const value = useContext(CounterContext);
  return (
    <CounterContext.Consumer>
      {(counter) => (
    <div className={styles.container}>
      ComponentF<br />
      Count: {value.count}<br />
      <button onClick={counter.increment}>+</button>
      <button onClick={counter.decrement}>-</button>
      <br></br>
      <ComponentG
        count={counter.count}
        increment={counter.increment}
        decrement={counter.decrement}
      />
    </div>
      )}
    </CounterContext.Consumer>
  );
}

export default ComponentF;
