import { createContext } from 'react';

const CounterContext = createContext({
    count: 0,
    increment: () => {},
    decrement: () => {},
});
//createContext adalah method, yang menerima object.
//object di atas merupakan value

CounterContext.displayName= 'CounterContext';
//displayName bertujuan untuk debugging

export default CounterContext;