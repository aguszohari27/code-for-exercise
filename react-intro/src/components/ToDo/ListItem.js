import React, { Component } from 'react';

class ListItem extends Component {
  handleClick = () => {
    const { index, handleDelete } = this.props;
    handleDelete(index);
  }

  render() {
    const { todo } = this.props;
    return (<li>{todo}<button onClick={this.handleClick}>Delete</button></li>);
  }
}

export default ListItem;