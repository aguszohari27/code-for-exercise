import React, { Component } from 'react';

import ListItem from './ListItem';

class ToDo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      todoInput: '',
      todoList: [
        'bangun tidur',
        'sarapan'
      ]
    }
  }

  componentDidMount() {
    console.log('App.js -- componentDidMount');
  }

  componentDidUpdate() {
    console.log('App.js -- componentDidUpdate');
  }

  componentWillUnmount() {
    console.log('ToDo/index.js -- componentWillUnmount');
  }

  handleChange = (event) => {
    this.setState({
      todoInput: event.target.value
    });
  }

  handleSubmit = (event) => {
    event.preventDefault();
    const { todoInput, todoList } = this.state;
    this.setState({
      todoList: [ ...todoList, todoInput ],
      todoInput: ''
    });
  }

  handleDelete = (index) => {
    const { todoList } = this.state;
    const temp = [ ...todoList ];
    temp.splice(index, 1);
    this.setState({
      todoList: temp
    });
  }

  render() {
    const { todoInput, todoList } = this.state;
    return (
    <div>
      <form onSubmit={this.handleSubmit}>
          <input
            placeholder="add new todo"
            value={todoInput}
            onChange={this.handleChange}
          />
          <button type="submit">Add</button>
        </form>
        {todoList.map((todo, index) => (
          <ListItem
            key={`${todo}-${index}`}
            index={index}
            todo={todo}
            handleDelete={this.handleDelete}
          />
        ))}
    </div>);
  }
}

export default ToDo;