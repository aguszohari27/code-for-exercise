import React, { Component } from 'react';

class Hello extends Component {
  componentWillUnmount() {
    console.log('Hello/index.js -- componentWillUnmount');
  }

  render() {
    const { username } = this.props;
    return (<div>Hello {username}</div>)
  }
}

export default Hello;