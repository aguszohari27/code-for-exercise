const result = document.querySelector('#result');
const getRandomInt = () => (Math.floor(Math.random() * 10));

const getFirstName = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (getRandomInt()) {
        resolve('Glints');
      } else {
        reject('Terjadi error');
      }
    }, 1000);
  });
}

const getLastName = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (getRandomInt()) {
        resolve('Academy');
      } else {
        reject('Terjadi error');
      }
    }, 1000);
  });
}

function handleClick() {
  getFirstName()
    .then((firstNameResponse) => {
      getLastName()
        .then((lastNameResponse) => {
          const fullName = `${firstNameResponse} ${lastNameResponse}`;

          result.innerText = fullName;
        }).catch((lastNameError) => {
          result.innerText = lastNameError;
        });
    })
    .catch((firstNameError) => {
      result.innerText = firstNameError;
    });
}