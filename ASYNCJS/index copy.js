// setTimeout(function() {
//    console.log('ini baris pertama');
// }, 3000);
// setTimeout(function() {
//    console.log('ini baris kedua');
// }, 2000);
// setTimeout(function() {
//    console.log('ini baris ketiga');
// }, 4000);

const result = document.querySelector('#result');

function getFirstName(callback) {
    setTimeout(() => {
        callback ('Glints');
    }, 1000);
}

function getLastName(callback) {
    setTimeout(() => {
        callback ('Academy');
    }, 1500);
}

function handleClick(){
    getFirstName((firstNameResponse) => {
        getLastName((lastNameResponse) => {
            const firstName = firstNameResponse;
            const lastName = lastNameResponse;
            const fullName = `${firstName} ${lastName}`;

            result.innerText = fullName;
        });
    });
}