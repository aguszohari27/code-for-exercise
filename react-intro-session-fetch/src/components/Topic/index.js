import React from "react";
import { useParams } from "react-router-dom";
import { ThemeContext } from "../../context";

const Topic = () => {
  let { title } = useParams();
  return (
    <ThemeContext.Consumer>
      {(value) => (<h3>Requested topic ID: {title} {value}</h3>)}
    </ThemeContext.Consumer>
  );
};

export default Topic;
