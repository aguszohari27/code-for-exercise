import React, { useEffect } from 'react';
import {
  Switch,
  Route,
  Link,
  useRouteMatch
} from 'react-router-dom';

import Topic from '../../components/Topic';

const Topics = () => {
  let match = useRouteMatch();
  console.log(match);

  return (
    <div>
      <h2>Topics</h2>

      <ul>
        <li>
          <Link to={`${match.url}/components`}>Components</Link>
        </li>
        <li>
          <Link to={`${match.url}/props-v-state`}>Props v. State</Link>
        </li>
        <li>
          <Link to={`${match.url}/javascript-es6`}>Javascript ES6</Link>
        </li>
      </ul>

      <Switch>
        <Route path={`${match.path}/:title`}>
          <Topic />
        </Route>
        <Route path={match.path}>
          <h3>Please select a topic.</h3>
        </Route>
      </Switch>
    </div>
  ) 
}

export default Topics;
