import { useState, useEffect } from 'react';
import axios from 'axios';

import style from './LocalWeather.module.css'

const options = {
  enableHighAccuracy: true,
  timeout: 5000,
  maximumAge: 0
}

const LocalWeather = () => {
  const [geolocationError, setGeolocationError] = useState(false);
  const [geolocationLoading, setGeolocationLoading] = useState(true);
  const [weatherLoading, setWeatherLoading] = useState(false);
  const [weatherError, setWeatherError] = useState(false);
  const [weatherData, setWeatherData] = useState({});

  useEffect(() => {
    navigator.geolocation.getCurrentPosition(handleGeolocationSuccess, handleGeolocationError, options);
  }, []);

  const handleGeolocationSuccess = (position) => {
    setGeolocationLoading(false);
    getLocalWeather(position.coords.latitude, position.coords.longitude);
  };

  const getLocalWeather = (latitude, longitude) => {
    setWeatherLoading(true);
    axios({
      url: 'https://fcc-weather-api.glitch.me/api/current',
      method: 'GET',
      params: {
        lat: latitude,
        lon: longitude
      }
    })
      .then((response) => {
        setWeatherData(response.data);
        setWeatherLoading(false);
      })
      .catch((error) => {
        setWeatherError(error);
      });
  }

  const handleGeolocationError = (error) => {
    setGeolocationError(error);
  };

  return (
    <div>
      {(geolocationLoading) && (
        <div>Getting current position...</div>
      )}
      {geolocationError && (<div>Failed to get current position</div>)}
      {weatherLoading && (<div>Getting local weather data</div>)}
      {weatherError && (<div>Failed to get local weather data</div>)}
      {(weatherData.name) && (
        <div>
          <div className={style.imageContainer}>
            <img src={weatherData.weather[0]?.icon} alt="current weather icon"/>
          </div>
          {weatherData.weather[0]?.main}<br />
          {weatherData.name}, {weatherData.sys.country}
        </div>
      )}
    </div>
  );
}

export default LocalWeather;
