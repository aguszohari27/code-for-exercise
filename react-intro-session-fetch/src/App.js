import React, { useState } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from 'react-router-dom';

import './styles/App.css';

import Hello from './pages/Hello';
import Todo from './pages/Todo';
import Topics from './pages/Topics';
import LocalWeather from './pages/LocalWeather';

import { ThemeContext } from './context';

const App = () => {
  const [username] = useState('');

  return (
    <ThemeContext.Provider value={'light'}>
      <div>
        <Router>
          <li>
            <Link to="/">
              Home
            </Link>
          </li>
          <li>
            <Link to="/hello">
              Hello
            </Link>
          </li>
          <li>
            <Link to="/todo">
              Todo App
            </Link>
          </li>
          <li>
            <Link to="/topics">
              Topics
            </Link>
          </li>
          <li>
            <Link to="/local-weather">
              Local Weather
            </Link>
          </li>

          <div className="container">
            <Switch>
              <Route path="/hello">
                <Hello username={username}></Hello>
              </Route>
              <Route path="/todo">
                <Todo />
              </Route>
              <Route path="/topics">
                <Topics />
              </Route>
              <Route>
                <LocalWeather />
              </Route>
              <Route path="/">
                <div>Home</div>
              </Route>
              <Route path="*">
                <div>Error 404: URL not found</div>
              </Route>
            </Switch>
          </div>
        </Router>
      </div>
    </ThemeContext.Provider>
  );
}

export default App;
