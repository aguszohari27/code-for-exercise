import { createStore, combineReducers } from 'redux';

import counterReducer from './counter';
import todoReducer from './todo';

const rootReducer = combineReducers ({ 
    counter: counterReducer, 
    todo: todoReducer
});

const store = createStore(rootReducer);

export default store;
