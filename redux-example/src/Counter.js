import { connect } from 'react-redux';

const Counter = (props) => {
    const handleClickAdd = () => {
        props.incrementCounter();
    }
    const handleClickMin = () => {
        props.decrementCounter();
    }
    const handleClickRes = () => {
        props.resetCounter();
    }
    
    return (
        <div>
        <div>count: {props.counter}</div>
        <button onClick={handleClickAdd}>+</button>
        <button onClick={handleClickMin}>-</button>
        <button onClick={handleClickRes}>reset</button>
        </div>
    )
}

const mapStateToProps = ({ counter }) => ({
    counter: counter.counter
});


const mapDispatchToProps = (dispatch) => ({
    incrementCounter: () => dispatch({ type: 'counter/increment' }),
    decrementCounter: () => dispatch({ type: 'counter/decrement' }),
    resetCounter: () => dispatch({ type: 'counter/reset' })
});

export default connect(mapStateToProps, mapDispatchToProps)(Counter);
