import { useState, useEffect } from "react";

const LocalWeather = () => {
    const [ coordinate, setCoordinate ] = useState ({
        loaded: false,
        coordinates: { ltd: "", lgtd: "" }
    });
    console.log(coordinate);

    const onSuccess = location => {
        setCoordinate({
            loaded: true,
            coordinates: {
                ltd: location.coords.latitude,
                lgtd: location.coords.longitude,
            },
        });
    };

    const onError = error => {
        setCoordinate({
            loaded: false,
            error,
        });
    }

    useEffect (() => {
        if( ! ("geolocation" in navigator ) ){
            onError({
                code: 0,
                message: "Geolocation is not supported",
            });
        }

        navigator.geolocation.getCurrentPosition(onSuccess, onError);
    }, []);

    return (
            <div>
                <h2>COBA LOKASI INI</h2>
            </div>
            );
};

// // }
// // const LocalWeather = () => {
//     // const [ error, setError ] = useState([]);

//     useEffect(() => {
//         navigator.geolocation.getCurrentPosition(
//             handleGeoLocationSuccess,
//             handleGeoLocationError,
//             handleGeoLocationOptions
//         )
//     }, []);

//     const handleGeoLocationSuccess = (pos) => {
//         let crd = pos.coords;
//         console.log('Anda sedang berada di:');
//         console.log(`Latitude: ${crd.latitude}`);
//         console.log(`Longitude: ${crd.longitude}`);
//         console.log(`More or less ${crd.accuracy} meters.`);
//         // setCoordinate({
//         //     ltd: crd.latitude,
//         //     lgtd: crd.longitude
//         // });
//     };

//     const handleGeoLocationError = () => {
//         let err = []
//         console.warn(`ERROR(${err.code}): ${err.message}`);
//     };

//     const handleGeoLocationOptions = () => ({
//         enableHighAccuracy: true,
//         timeout: 3000,
//         maximumAge: 0
//     });

// // ketika dia berhasil mengambil location, simpan location di state
// // apabila sukses terpanggil, masukkan success ke state

export default LocalWeather;