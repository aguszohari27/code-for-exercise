import React, { useState } from 'react';

import ListItem from '../../components/Todo/ListItem';

const Todo = () => {
  const [ todoList, setTodoList ] = useState([]);
  const [ todoInput, setTodoInput ] = useState('');

  const handleChange = (event) => {
    setTodoInput(event.target.value);
  }

  const handleSubmit = (event) => {
    event.preventDefault();
    if (todoInput) {
      setTodoList([ ...todoList, todoInput ]);
      setTodoInput('');
    }
  }

  const handleDelete = (index) => {
    const temp = [ ...todoList ];
    temp.splice(index, 1);
    setTodoList(temp);
  }

    return (
    <div>
      <form onSubmit={handleSubmit}>
          <input
            placeholder="add new todo"
            value={todoInput}
            onChange={handleChange}
          />
          <button type="submit">Add</button>
        </form>
        {todoList.map((todo, index) => (
          <ListItem
            key={`${todo}-${index}`}
            index={index}
            todo={todo}
            handleDelete={handleDelete}
          />
        ))}
    </div>);
  }

export default Todo;
