import React, { useEffect } from 'react';

const Hello = (props) => {
  useEffect(() => {
    return function cleanup() {
      console.log('Hello/index.js -- componentWillUnmount');
    }
  }, [])

  return (<div>Hello {props.username}</div>)
}

export default Hello;
