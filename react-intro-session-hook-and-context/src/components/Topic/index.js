import React from "react";
import { useParams } from "react-router-dom";

const Topic = () => {
  let { title } = useParams();
  return <h3>Requested topic ID: {title}</h3>;
};

export default Topic;
