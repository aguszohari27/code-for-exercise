import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from 'react-router-dom';

import './App.css';

import Hello from './pages/Hello';
import Todo from './pages/Todo';
import Topics from './pages/Topics';
import WikipediaViewer from './pages/WikipediaViewer';
import LocalWeather from './pages/LocalWeather';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: 'Agus',
    }
  }

  componentDidMount() {
    console.log('App.js -- componentDidMount');
  }

  componentDidUpdate() {
    console.log('App.js -- componentDidUpdate');
  }

  render() {
    const { username } = this.state;
    return ( 
      <div>
        <Router>
          <li>
            <Link to="/">
              Top
            </Link>
          </li>
          <li>
            <Link to="/hello">
              Hello
            </Link>
          </li>
          <li>
            <Link to="/todo">
              Todo App
            </Link>
          </li>
          <li>
            <Link to="/topics">
              Topics
            </Link>
          </li>
          <li>
            <Link to="/wikipediaviewer">
              Wikipedia Viewer
            </Link>
          </li>
          <li>
            <Link to="/localweather">
              Local Weather
            </Link>
          </li>

          <div className="container">
            <Switch>
              <Route path="/hello">
                <Hello username={username}></Hello>
              </Route>
              <Route path="/todo">
                <Todo />
              </Route>
              <Route path="/topics">
                <Topics />
              </Route>
              <Route path="/wikipediaviewer">
                <WikipediaViewer />
              </Route>
              <Route path="/localweather">
                <LocalWeather />
              </Route> 
              <Route path="/">
                <div></div>
              </Route>
              <Route path="*">
                <div>Error 404: URL not found</div>
              </Route>
            </Switch>
          </div>
        </Router>
      </div>
    );
  }
}

export default App;
