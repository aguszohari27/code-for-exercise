import React from "react";
import { Link } from "react-router-dom";

const Nav = () => {
    return (
        <div style={{ display: "flex", flexDirection: "column" }}>
            <Link to="/movie/overview">Overview</Link>
            <Link to="/movie/review">Review</Link>
            <Link to="/movie/character">Character</Link>
            <Link to="/movie/soundtrack">Soundtrack</Link>
        </div>
    );
};

export default Nav;