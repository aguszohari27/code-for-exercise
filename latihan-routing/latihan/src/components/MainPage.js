import React from "react";
import Nav from "./Nav";
import Character from "./Characters";
import Review from "./Review";
import Overview from "./Overview";
import Soundtrack from './Soundtrack';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

const MainPage = () => {
    return (
        <div>
            <Router>
                <Nav />
            <div>
                <Switch>
                    <Route exact path="/movie/overview" component={Overview} />
                    <Route exact path="/movie/review" component={Review} />
                    <Route exact path="/movie/character" component={Character} />
                    <Route exact path="/movie/soundtrack" component={Soundtrack} />
                </Switch>
                </div>
            </Router>
        </div>
    );
};

export default MainPage;