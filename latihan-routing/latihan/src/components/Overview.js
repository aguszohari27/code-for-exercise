import React from "react";

const Overview = () => {
    return <div className="container">
        <h2>Synopsis</h2>
        <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec non lobortis lectus. In eleifend volutpat dolor, eget elementum nisi viverra in. Sed sit amet ipsum sed nisi aliquet sollicitudin eu non tortor. Pellentesque urna enim, dapibus venenatis lacus sit amet, auctor consectetur urna. Morbi semper auctor purus, sit amet pulvinar nulla venenatis sit amet. In tristique, est sit amet pellentesque volutpat, nibh neque tincidunt erat, ac pretium eros massa quis lacus. Integer scelerisque ex pulvinar velit scelerisque tincidunt. Proin nec hendrerit tortor. Vestibulum interdum dapibus ligula eu tempor. Integer tristique lacinia lectus, at feugiat tortor ultrices sed. Donec pulvinar porta dolor a interdum. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque in est nisi. Maecenas eleifend felis et luctus congue.
        </p>
        
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec non lobortis lectus. In eleifend volutpat dolor, eget elementum nisi viverra in. Sed sit amet ipsum sed nisi aliquet sollicitudin eu non tortor. Pellentesque urna enim, dapibus venenatis lacus sit amet, auctor consectetur urna. Morbi semper auctor purus, sit amet pulvinar nulla venenatis sit amet. In tristique, est sit amet pellentesque volutpat, nibh neque tincidunt erat, ac pretium eros massa quis lacus. Integer scelerisque ex pulvinar velit scelerisque tincidunt. Proin nec hendrerit tortor. Vestibulum interdum dapibus ligula eu tempor. Integer tristique lacinia lectus, at feugiat tortor ultrices sed. Donec pulvinar porta dolor a interdum. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque in est nisi. Maecenas eleifend felis et luctus congue.
            </p>
            
        <h3>Movie Info:</h3>
        
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec non lobortis lectus. In eleifend volutpat dolor, eget elementum nisi viverra in. Sed sit amet ipsum sed nisi aliquet sollicitudin eu non tortor. Pellentesque urna enim, dapibus venenatis lacus sit amet, auctor consectetur urna. Morbi semper auctor purus, sit amet pulvinar nulla venenatis sit amet. In tristique, est sit amet pellentesque volutpat, nibh neque tincidunt erat, ac pretium eros massa quis lacus. Integer scelerisque ex pulvinar velit scelerisque tincidunt. Proin nec hendrerit tortor. Vestibulum interdum dapibus ligula eu tempor. Integer tristique lacinia lectus, at feugiat tortor ultrices sed. Donec pulvinar porta dolor a interdum. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque in est nisi. Maecenas eleifend felis et luctus congue.
            </p>
        
        <h4>Release Date:</h4>
        
        <p>January, 20 1998</p>
        
        </div>;
};

export default Overview;