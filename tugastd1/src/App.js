import './App.css';

import Todo from './Todo';

function App() {
  return (
    <div className="App">
      <Todo App/>
    </div>
  );
}

export default App;
