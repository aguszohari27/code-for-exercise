const initialState = {
    todolist: [],
};

const reducer = (state= initialState, action) => {
    switch (action.type) {
        case 'ADD_TODO':
            return {
                ...state,
                todolist: [...state.todolist, 
                    action.payload],
            };
        case 'DELETE_TODO':
            return {
                ...state,
                todolist: state.todolist.filter(todo => todo.id !== action.payload), 
            };
        default:
            return state;
    }
}

export default reducer;