import { useState } from 'react';
import { connect } from 'react-redux';

const Todo = (props) => {
  const [ input, setInput ] = useState('');
  const handleSubmit = (event) => {
    event.preventDefault();
    if (input) {
      props.addTodo(input);
      setInput('');
    } else {
      alert('Try again');
    }
  }

  const handleDelete = (index) => {
    props.removeTodo(index);
  }

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <input
          onChange={(e) => setInput(e.target.value)}
          placeholder="Write here"
          value={input}
        />
        <button>ADD</button>
      </form>
      <ol>
        {props.todo.map((value, index) => (
          <li key={`${value}-${index}`}>
            {value} <button onClick={() => { handleDelete(index) }}>DEL</button>
          </li>
        ))
        }
      </ol>
    </div>
  )
}

const mapStateToProps = ({ todo }) => ({
  todo: todo.todo});

const mapDispatchToProps = (dispatch) => ({
  addTodo: (todo) => { dispatch({ 
    type: 'ADD_TODO', 
    payload: { todo } 
  }) },
  removeTodo: (index) => { dispatch({ 
    type: 'DELETE_TODO',
    payload: { index } }) }
});

export default connect(mapStateToProps, mapDispatchToProps)(Todo);