const initialState = {
  todo: [
    'Bangun tidur'
  ]
};

const todoReducer = (state = initialState, action) => {
  switch(action.type) {
    case 'ADD_TODO':
      return {
        ...state,
        todo: state.todo.concat(action.payload.todo)
      }
    case 'DELETE_TODO':
      const temp = state.todo.slice();
      temp.splice(action.payload.index, 1);
      return {
        ...state,
        todo: temp
      }
    default:
      return state;
  }
}

export default todoReducer;
