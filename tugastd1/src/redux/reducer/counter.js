const initialState = {
  counter: 0,
}

const counterReducer = (state = initialState, action) => {
  switch(action.type) {
    case 'COUNTER_INCREMENT':
      return {
        ...state,
        counter: state.counter + 1
      }
    case 'COUNTER_DECREMENT':
      return {
        ...state,
        counter: state.counter - 1
      }
    case 'COUNTER_RESET':
      return {
        ...state,
        counter: 0
      }
    default:
      return state;
  }
}

export default counterReducer;
